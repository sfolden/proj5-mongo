# Project 5 Brevet calculator (Flask + ajax + mongodb)
 Author: Shane Folden


This is a Brevet Controle Time Calculator which uses ACP specifications.

Table: https://rusa.org/octime_alg.html
ACP brevet Calculator: https://rusa.org/octime_acp.html

Two buttons have been added - submit and display.

Submit writes values in the table to a mongodb database

Display opens a new page and calls the mongodb database to display the values

# button testing

Submit pressed with no values:

If submit is pressed with no values an alert will pop up telling the user to enter values before submitting.
Additionally no data will be written or overwritten in the mongodb database


Submit pressed twice in a row:

After submit is pressed, I clear the inputs in the displayed table to prevent this error as much as possible.
If the user presses submit a second time after they are returned to the blank table, see above paragraph for expected behavior ^^
If they repopulate it with the same values or new values, those values will replace whatever existed already in the database.

Display is pressed with an empty database:

If display is pressed and no values have been submitted by the user, the user is redirected to the index



#Calculator logic:
The way this calculator works is by taking the control distance and dividing by either the min/max max_speeds

ie 60km
max speed at 60 = 34 kph
min speed at 60 = 15 kph

Open time = 60/34    1:45:35
closing time = 60/15 = 4:00:00

If the speed falls over multiple ranges, then each range is calculated and added together

ie 300km

max speed from 0-200 = 34 kph
max speed from 200-400 = 32 kph
min speed from 0-600 = 15 kph

open time = 200/34 + 100/32 hr
close time = 300/15 hr
