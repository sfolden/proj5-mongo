"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
from pymongo import MongoClient
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import os


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
#uri = CONFIG.MONGO_URI

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db.tododb.delete_many({})



###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_submit_times", methods=['POST'])
def _submit_times():
    count = 0



    print("this the length pre ")

    print("that was it")
    km = request.form.getlist("km")
    miles = request.form.getlist("miles")
    ends = request.form.getlist("close")
    starts = request.form.getlist("open")
    for j in miles:
        if j != "":
            count = count + 1
    if count > 0:
        print( len(miles))
        db.tododb.delete_many({})
        for k in km:
            print(k)
        for l in starts:
            print(l)
        for j in ends:
            if j != "":
                print(j)

        for i in range(len(miles)):
            if miles[i] != "":
                db.tododb.insert_one({ "miles": miles[i], "km": km[i], "start_time": starts[i], "end_time": ends[i]})
    return redirect('/index')





@app.route("/_display_times", methods=['POST', 'GET'])
def _display_times():
    _items = db.tododb.find()
    if db.tododb.find().count() == 0:
        return redirect('/index')

    items = [item for item in _items]

    return render_template('todo.html', items=items)




@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    print("this is before everything")
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brev_dist = request.args.get('brev_dist', type=int)
    beginning_time = request.args.get('beginning_time', type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))


    open_time = acp_times.open_time(km, brev_dist, beginning_time)
    close_time = acp_times.close_time(km, brev_dist, beginning_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
